<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>welcome</title>
<link rel="stylesheet" type="text/css" href="/static/css/common/base.css?v=1.0.0" />
<script type="text/javascript" charset="utf-8" src="/static/js/common/base.js?v=1.0.0"></script>
</head>
<body>
	<h1 class="title">欢迎你来到我的世界！</h1>
	<div class="welcome-logo">
		<img alt="LOGO" src="/static/image/common/friends-logo.png">
	</div>
	<c:forEach begin="0" end="10">
		<p>我是这个页面的内容</p>
	</c:forEach>
</body>
</html>