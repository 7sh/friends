package com.qsh.scfw.friends.main.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qsh.scfw.dao.entity.SysUser;
import com.qsh.scfw.dao.mapper.SysUserMapper;
import com.qsh.scfw.friends.main.service.UserService;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private SysUserMapper sysUserMapper;
	
	@Override
	public int insertUser(SysUser sysUser) {
		
		int result = sysUserMapper.insert(sysUser);
		
		return result;
	}

}
