package com.qsh.scfw.friends.main.utils;

/**
 * 作者：綦洋
 * 创建时间：2016年4月1日上午10:29:38
 * 功能描述：字符串工具类。
 */
public class StringUtil {
	
	/**
	 * 作者:綦洋
	 * 创建时间：2016年4月1日10:18:42
	 * 方法描述：判断用户名是否是电话号码。
	 */
	public static boolean isMobile(String mobile) {
		if ( null==mobile ) {
			return false;
		}
//		String phoneRegex = "((1[3,4,5,8][0-9]{1})+\\d{8})";
		String phoneRegex = "[1][0-9]{10}";
		if(mobile.matches(phoneRegex)){
			return true;
		}
		return false;
	}
	
}
