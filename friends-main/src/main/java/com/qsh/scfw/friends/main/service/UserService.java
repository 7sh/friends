package com.qsh.scfw.friends.main.service;

import com.qsh.scfw.dao.entity.SysUser;

public interface UserService {
	
	public int insertUser(SysUser sysUser);
	
}
