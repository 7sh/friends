package com.qsh.scfw.friends.main.controller;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.qsh.scfw.dao.entity.SysUser;
import com.qsh.scfw.friends.main.service.UserService;

@Controller
public class UserController {
	
	private Logger log = org.slf4j.LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/index")
	public String index(){
		
		SysUser user = new SysUser();
		user.setTrueName("星期二");
		user.setUserName("xqe");
		user.setUserPwd("123456");
		
		int result = userService.insertUser(user);
		
		log.debug(">>>>>恭喜你，打通了："+result);
		
		return "index";
	}
	
	@RequestMapping("/welcome")
	public String welcome(){
		
		log.debug(">>>>>进入欢迎页面");
		
		return "friends/main/welcome";
	}
	
	@RequestMapping("/notification")
	public String notification(){
		
		log.debug(">>>>>桌面通知");
		
		return "friends/main/notification";
	}
	
	
}
